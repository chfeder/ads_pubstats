### ADS Publication Statistics Scraper ###

This repository contains a simple python script, pubstats.py, that
scrapes the ADS to grab publication statistics for a set of
institutions, which can then be compared. 

### Requirements ###

* numpy
* matplotlib

### Input Data ###

To use this script, you provide a list of text files containing the
people at each institution whose publications statistics you'd like to
see. Each file is formatted as follows 

    Institution_Name
    Output_Name
    Person_1
    Person_2
    Person_3
    ...

where Insitution_Name is the name of the institution (which will be
used to label it in any plots), Output_Name is the name that will be
used as a base for output files specific to that institution, and
Person_N is list of personnel at that institution who should be
included in the search. The Person_N lines are formatted as 

    First_Name, Last_Name   [ ; ADS_First_Name_1, ADS_Last_Name_1 ; ... ]

where First_Name and Last_Name are the first and last name of the
person as they will appear in the outputs, and the items in brackets
are optional. If the optional information is not specified, then ADS
will be queried the specified first and last name, without exact
author name matching turned on. You may also provide any number of ADS
search names for each person, which follow the initial first name,
last name pair, and are separated by semi-colons. If these are
specified, the script will do an ADS search with exact name matching
for each of the first name, last name pairs you provide, combining
them with a logical or.

For example, the line:

    John Q., Public

causes an ADS query to be executed with last name Public, first name
John Q., without exact author name matching. In comparison, the line: 

    Jane Q., Public;  J. Q., Public;  Jane, Public

causes an ADS query to be executed with the last name Public and the
first name J. Q., or the last name Public and the first name Jane with
exact name matching turned on. Note that in this example the search
will not include the combination of last name Public, first name Jane
Q. 

### Outputs ###

The script computes the following citation metrics:

* total refereed publications
* total citations (on both refereed and non-refereed publications)
* normalized citations (citations divided by number of authors on each
  paper) 
* H index (= number N of papers with at least N citations)
* M index (H index divided by time in years between first and last
  publication) 
* Hnorm index (H index on normalized citations)
* Mnorm index (M index on normalised citations)

From these statistics, the code can optionally produce a number of
outputs:

* For each institution, the value of each metric for each person
  listed for that institution -- output in text format as
  Output_Name_report.txt
* For each metric, a ranked list for each institution, from highest
  value of that metric at that institution -- output in text format,
  as summary_metric.txt, where metric indicates which metric is listed
* Plots showing the distributions of each metric at each institution,
  both distributed by person, and normalised by total number of staff
  -- output as summary_metric.pdf

Default behavior is to compute all quantities over the full publishing
career of each person, but the script can also compute metrics only
for a specified range in years (see below under options). If this
option is selected, the person-by-person report will include both
lifetime and year-truncated statistics, and for each
summary_metric.txt or summary_metric.pdf file, there will be a
summary_yr_metric.txt and summary_yr_metric.pdf file, which contains
the same quantities but only considering publications within the
specified years.


### Options ###

The script takes as arguments the list of text files to be
processed. It also supports the following options:

* `--outfile OUTFILE` : changes the name of the summary output files
  from summary_* to OUTFILE_*
* `--overwrite` : by default, the script caches the ADS data it
  downloads, so that additional analysis or output does not require
  re-downloading the data. This option overrides that behavior, so
  that the cached data are ignored and overwritten in favor of doing a
  fresh ADS query.
* `--fulltables` : by default the script does not write out the
  individual institution reports containing person-by-person
  publication statistics (the *_report.txt files described
  above). This option causes those files to be written.
* `--Hmin HMIN` : excludes people with H index less than HMIN to be
  excluded from the analysis
* `--nretry NRETRY` : number of times to retry an ADS query if the
  query returns an HTTP Error (which happens periodically); default is
  5
* `--plot` : causes the PDF plot files to be created; default behavior
  is only to write the summary_metric.txt files
* `--plotemphasize` : when making plot files, this option causes the
  first institution listed to be emphasized in the plots, by drawing
  it with a thicker line
* `--years YEARSTART YEAREND` : starting and ending years of interest
  (inclusive); if specified, outputs are produced including only the
  range of years specified, as well as the lifetime measures
* `--verbose` : makes the script produce verbose output when running

### License ###

This code is distributed under the GPL v3.0 license. A copy of the
license file is included in the repository.