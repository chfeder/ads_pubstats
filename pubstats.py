# A script to scrape ADS and get publications statistics for a list of
# institutions

import urllib2
import re
import argparse
import HTMLParser
import pickle
import numpy as np
import matplotlib.pyplot as plt
import warnings

# Parse the input list of instituions
parser = argparse.ArgumentParser(
    description="Publications statistics calculator.")
parser.add_argument("files", metavar="files", type=str, nargs="+",
                    help="list of personnel files to process")
parser.add_argument("-o", "--outfile", type=str, default="summary",
                    help="base name of summary output files;"
                    " default = summary")
parser.add_argument("-ov", "--overwrite", action="store_true",
                    default=False,
                    help="overwrite existing data; default is "
                    "to use stored data if they are found")
parser.add_argument("-ft", "--fulltables", action="store_true",
                    default=False,
                    help="write table output for all institutions; "
                    "default behavior is to write comparisons between "
                    "institutions only")
parser.add_argument("-Hm", "--Hmin", type=int, default=0,
                    help="drop people with H < Hmin from analysis")
parser.add_argument("-nr", "--nretry", type=int,
                    default=5,
                    help="number of times to retry on HTTP errors")
parser.add_argument("-p", "--plot", action="store_true",
                    default=False,
                    help="make summary plots")
parser.add_argument("-pe", "--plotemphasize", action="store_true",
                    default=False,
                    help="in summary plots, emphasize the first"
                    " institution listed by using a thicker line")
parser.add_argument("-y", "--years", nargs=2, default=None,
                    help="in addition the lifetime analysis report "
                    "statistics only within the range in years "
                    "specified; must specify both a starting and an "
                    "ending year; years are inclusive")
parser.add_argument("-v", "--verbose", action="store_true",
                    default=False,
                    help="produce verbose output while running")
args = parser.parse_args()

# ADS magic search strings
allstr = "http://adsabs.harvard.edu/cgi-bin/nph-abs_connect?"\
         "db_key=AST&db_key=PRE&qform=AST&arxiv_sel=astro-ph&"\
         "arxiv_sel=cond-mat&arxiv_sel=cs&arxiv_sel=gr-qc&"\
         "arxiv_sel=hep-ex&arxiv_sel=hep-lat&arxiv_sel=hep-ph&"\
         "arxiv_sel=hep-th&arxiv_sel=math&arxiv_sel=math-ph&"\
         "arxiv_sel=nlin&arxiv_sel=nucl-ex&arxiv_sel=nucl-th&arxiv_sel"\
         "=physics&arxiv_sel=quant-ph&arxiv_sel=q-bio&sim_query=YES&"\
         "ned_query=YES&adsobj_query=YES&"\
         "NAME_QUERY_STRING&object=&start_mon=&start_year=&" \
         "end_mon=&end_year=&ttl_logic=OR&title=&txt_logic=OR&text=&"\
         "nr_to_return=100000&start_nr=1&jou_pick=ALL&ref_stems=&"\
         "data_and=ALL&group_and=ALL&start_entry_day=&start_entry_mon=&"\
         "start_entry_year=&end_entry_day=&end_entry_mon=&"\
         "end_entry_year=&min_score=&sort=CITATIONS&data_type=SHORT&"\
         "aut_syn=YES&ttl_syn=YES&txt_syn=YES&aut_wt=1.0&obj_wt=1.0&"\
         "ttl_wt=0.3&txt_wt=3.0&aut_wgt=YES&obj_wgt=YES&ttl_wgt=YES&"\
         "txt_wgt=YES&ttl_sco=YES&txt_sco=YES&version=1"    
refstr = "http://adsabs.harvard.edu/cgi-bin/nph-abs_connect?db_key="\
         "AST&db_key=PRE&qform=AST&arxiv_sel=astro-ph&arxiv_sel="\
         "cond-mat&arxiv_sel=cs&arxiv_sel=gr-qc&arxiv_sel=hep-ex&"\
         "arxiv_sel=hep-lat&arxiv_sel=hep-ph&arxiv_sel=hep-th&"\
         "arxiv_sel=math&arxiv_sel=math-ph&arxiv_sel=nlin&arxiv_sel="\
         "nucl-ex&arxiv_sel=nucl-th&arxiv_sel=physics&arxiv_sel="\
         "quant-ph&arxiv_sel=q-bio&sim_query=YES&ned_query=YES&"\
         "adsobj_query=YES&NAME_QUERY_STRING&object=&start_mon=&"\
         "start_year=&end_mon=&end_year=&ttl_logic=OR&title=&"\
         "txt_logic=OR&text=&nr_to_return=100000&start_nr=1&"\
         "jou_pick=NO&ref_stems=&data_and=ALL&group_and=ALL&"\
         "start_entry_day=&start_entry_mon=&start_entry_year=&"\
         "end_entry_day=&end_entry_mon=&end_entry_year=&"\
         "min_score=&sort=CITATIONS&data_type=SHORT&aut_syn=YES&"\
         "ttl_syn=YES&txt_syn=YES&aut_wt=1.0&obj_wt=1.0&ttl_wt=0.3&"\
         "txt_wt=3.0&aut_wgt=YES&obj_wgt=YES&ttl_wgt=YES&txt_wgt="\
         "YES&ttl_sco=YES&txt_sco=YES&version=1"

# Routine to parse a file
def parse_personnel(fname):

    # Open file and read header
    fp = open(fname, 'r')
    institution = fp.readline().strip()
    outfile = fp.readline().strip()
    
    # Read personnel list
    personnel = []
    for line in fp:

        try:
        
            # Read name of person
            spl = line.split(",")
            gname = spl[0].strip()
            sname = spl[1].split(";")[0].strip()
            fullname = gname + " " + sname

            # See if there are any semi-colons, indicating that we
            # need to use custom search strings
            if ";" in line:

                # Yes, custom search strings exist
                ads_gname = []
                ads_sname = []
                spl1 = line.split(";")
                for s in spl1[1:]:

                    # Grab names
                    spl2 = s.split(",")
                    ads_gname.append(spl2[0].strip())
                    ads_sname.append(spl2[1].strip())

            else:
                ads_gname = None
                ads_sname = None

        except IndexError:
            print("Badly formatted line: "+line)
            raise IndexError

        # Save person
        personnel.append({
            "Fullname" : fullname,
            "Surname" : sname,
            "Given Name" : gname,
            "ADS Surname" : ads_sname,
            "ADS Given Name" : ads_gname})

    # Close
    fp.close()

    # Return
    return institution, outfile, personnel


# Function that takes a list of citation counts and years, and returns
# the H and M indices for them
def HM(cites, yr):
    citessort = np.copy(cites)
    citessort.sort()
    citessort = citessort[::-1]
    if len(citessort) == 0:
        H = 0
        M = 0.0
    elif len(citessort) > np.amin(citessort):
        H = np.argmax(citessort < np.arange(len(citessort))+1)
        M = float(H) / (np.amax(yr) - np.amin(yr) + 1)
    else:
        H = len(citessort)
        M = float(H) / (np.amax(yr) - np.amin(yr) + 1)
    return H, M


# Loop over files to process
citedata = []
hparser=HTMLParser.HTMLParser()
for f in args.files:

    # Read the personnel list
    institution, outfile, personnel = parse_personnel(f)
    stats = { "Institution" : institution,
              "Outfile" : outfile,
              "Personnel" : personnel }

    # See if we have already processed this institution; if we have,
    # read the data for it
    if not args.overwrite:
        try:
            fp = open(stats["Outfile"]+".pkl", "rb")
            data = pickle.load(fp)
            fp.close()
            fullnames = [s["Fullname"] for s in data["Personnel"]]
            for p in stats["Personnel"]:
                if p["Fullname"] in fullnames:
                    idx = fullnames.index(p["Fullname"])
                    p["Bibcodes"] = data["Personnel"][idx]["Bibcodes"]
                    p["Year"] = data["Personnel"][idx]["Year"]
                    p["Refereed"] = data["Personnel"][idx]["Refereed"]
                    p["Citations"] = data["Personnel"][idx]["Citations"]
                    p["Normalised Citations"] \
                        = data["Personnel"][idx]["Normalised Citations"]
                    p["Journal"] = data["Personnel"][idx]["Journal"]
            if args.verbose:
                print("Successfully ingested "+stats["Outfile"]+".pkl")
        except (IOError, KeyError) as e:
            pass

    # Loop over personnel
    for p in stats["Personnel"]:

        # Skip people we've already read
        if "Journal" not in p.keys():

            # If verbose print status
            if args.verbose:
                print("Doing ADS query for "+p["Fullname"])

            # Construct ADS query
            if p["ADS Surname"] is None:
            
                # Names that do not require special handling
                nameqstr = "aut_logic=OR&obj_logic=OR&author=" + \
                           p["Surname"].replace(" ", "+") + "%2C+" +\
                           p["Given Name"].replace(" ", "+")

            else:

                # Names that do require special handling
                nameqstr = "aut_xct=YES&aut_logic=OR&obj_logic=OR&author="
                for s, g in zip(p["ADS Surname"], p["ADS Given Name"]):
                    nameqstr += s.replace(" ", "+")+"%2C"+\
                                g.replace(" ", "+")+ "%0D%0A"
                nameqstr = nameqstr[:-6]
            searchstr = refstr.replace("NAME_QUERY_STRING", nameqstr)

            # Execute ADS query to get paper list
            req = urllib2.Request(searchstr)
            try:
                handle = urllib2.urlopen(req, timeout=120)
                data = ''.join(handle.read())
            except urllib2.HTTPError as e:
                warnings.warn("urllib2 says: {:s}; retrying {:d} times".
                              format(e.strerror), args.nretry)
                for i in range(args.nretry):
                    try:
                        time.sleep(10)
                        handle = urllib2.urlopen(req, timeout=120)
                        data = ''.join(handle.read())
                        break
                    except urllib2.HTTPError:
                        pass
                if i == args.nretry:
                    raise urllib2.HTTPError

            # Extract list of bibcodes
            bibcodes = re.findall('name="bibcode" value="(.*?)"', data)
            refereed = [True]*len(bibcodes)

            # Mark PhD theses as non-refereed
            for k, b in enumerate(bibcodes):
                if 'PhDT' in b:
                    refereed[k] = False

            # Extract number of citations
            citestr = re.findall(
                '<td align="left" valign="baseline">(\d*)\.000', data)
            cites = [ int(float(c)) for c in citestr ]
 
            # Grab number of authors
            authliststr = re.findall(
                '<td align="left" valign="top" width="25%">'
                '(.*?)</td><td><br></td>'
                '<td align="left" valign="top" colspan=3>',
                data)
            nauth = [ len(a.split("; ")) for a in authliststr ]
            for i in range(len(nauth)):
                coauth = re.findall('and (.*?) coauthors', authliststr[i])
                if len(coauth) != 0:
                    # minus one because the 'coauthors' is one to many after a.split("; ")
                    nauth[i] += int(coauth[0])-1

            # Now grab the list of all publications, refereed or not;
            # process as with the refereed ones
            searchstr = allstr.replace("NAME_QUERY_STRING", nameqstr)
            req = urllib2.Request(searchstr)
            try:
                handle = urllib2.urlopen(req, timeout=120)
                data = ''.join(handle.read())
            except urllib2.HTTPError as e:
                warnings.warn("urllib2 says: {:s}; retrying {:d} times".
                              format(e.strerror), args.nretry)
                for i in range(args.nretry):
                    try:
                        time.sleep(10)
                        handle = urllib2.urlopen(req, timeout=120)
                        data = ''.join(handle.read())
                        break
                    except urllib2.HTTPError:
                        pass
                if i == args.nretry:
                    raise urllib2.HTTPError
            bibcodes_all = re.findall('name="bibcode" value="(.*?)"', data)
            citestr_all = re.findall(
                '<td align="left" valign="baseline">(\d*)\.000', data)
            cites_all = [ int(float(c)) for c in citestr_all ]
            authliststr = re.findall(
                '<td align="left" valign="top" width="25%">'
                '(.*?)</td><td><br></td>'
                '<td align="left" valign="top" colspan=3>',
                data)
            nauth_all = [ len(a.split("; ")) for a in authliststr ]
            for i in range(len(nauth_all)):
                coauth = re.findall('and (.*?) coauthors', authliststr[i])
                if len(coauth) != 0:
                    # minus one because the 'coauthors' is one to many after a.split("; ")
                    nauth_all[i] += int(coauth[0])-1

            # Add non-refereed publications to the list of bibcodes
            for b, c, n in zip(bibcodes_all, cites_all, nauth_all):
                if b not in bibcodes:
                    bibcodes.append(b)
                    refereed.append(False)
                    cites.append(c)
                    nauth.append(n)

            # For each publication, store the bibcode, journal, year,
            # refereed status
            bibcodes = np.array(bibcodes)
            refereed = np.array(refereed)
            nauth = np.array(nauth)
            cites = np.array(cites)
            normcites = np.array(cites, dtype=float) / nauth
            year = np.array([int(b[:4]) for b in bibcodes])
            journal = np.array([hparser.unescape(b[4:b.find('.')]).
                                replace("%26", "&")
                                for b in bibcodes])
            p["Bibcodes"] = bibcodes
            p["Refereed"] = refereed
            p["Year"] = year
            p["Journal"] = journal
            p["Citations"] = cites
            p["Normalised Citations"] = normcites

            # Print status if verbose
            if args.verbose:
                print("Found {:d} refereed publications, ".
                      format(int(np.sum(refereed))) +
                      "{:d} total publications".format(int(len(bibcodes))))

    # Save
    fp = open(stats["Outfile"]+".pkl", "wb")
    pickle.dump(stats, fp)
    fp.close()
    citedata.append(stats)

# Compute statistics on a person by person basis
for c in citedata:
    for p in c["Personnel"]:
        
        # Sum citation counts
        p["Total Citations"] = int(np.sum(p["Citations"]))
        p["Total Normalised Citations"] \
            = np.sum(p["Normalised Citations"])

        # Sum citation counts in year range of interest
        if args.years is not None:
            idx = np.logical_and(p["Year"] >= int(args.years[0]),
                                 p["Year"] <= int(args.years[1]))
            p["Year Total Citations"] = int(np.sum(p["Citations"][idx]))
            p["Year Total Normalised Citations"] \
                = np.sum(p["Normalised Citations"][idx])
        
        # Compute H and M indices
        p["H"], p["M"] = HM(p["Citations"], p["Year"])
        p["Hnorm"], p["Mnorm"] = HM(p["Normalised Citations"],
                                    p["Year"])
        if args.years is not None:
            idx = np.logical_and(p["Year"] >= int(args.years[0]),
                                 p["Year"] <= int(args.years[1]))
            p["Year H"], p["Year M"] = HM(p["Citations"][idx],
                                          p["Year"][idx])
            p["Year Hnorm"], p["Year Mnorm"] \
                = HM(p["Normalised Citations"][idx],
                     p["Year"][idx])

# Output text summary for each institution if requested
if args.fulltables:
    for c in citedata:

        # Open file
        fp = open(c["Outfile"]+"_report.txt", "w")

        # Write
        fp.write(("{:<18s} {:<18s}  {:>6s}  {:>5s}  {:>8s}  {:>3s}"
               "  {:>5s}  {:>5s}  {:>5s}").format(
                   "Surname", "GivenName", "RefPub", "Cites",
                   "NCites", "H", "M", "Hnorm", "Mnorm"))
        if args.years is not None:
            fp.write(
                "  {:>8s}  {:>8s}  {:>9s}  {:>3s}  {:>5s}  {:>7s}  {:>7s}".
                format("YrRefPub", "YrCites", "YrNCites", "YrH", "YrM",
                       "YrHNorm", "YrMNorm"))
        fp.write("\n")
        for p in c["Personnel"]:
            fp.write(("{:<18s} {:<18s}  {:>6d}  {:>5d}  {:>8.2f}  {:>3d}"
                      "  {:>5.2f}  {:>5d}  {:>5.2f}").format(
                          p["Surname"], p["Given Name"],
                          int(np.sum(p["Refereed"])),
                          p["Total Citations"],
                          p["Total Normalised Citations"],
                          p["H"], p["M"], p["Hnorm"], p["Mnorm"]))
            if args.years is not None:
                idx = np.logical_and(p["Year"] >= int(args.years[0]),
                                     p["Year"] <= int(args.years[1]))
                fp.write(("  {:>8d}  {:>8d}  {:>9.2f}  {:>3d}  {:>5.2f}"
                          "  {:>7d}  {:>7.2f}").format(
                              int(np.sum(p["Refereed"][idx])),
                              p["Year Total Citations"],
                              p["Year Total Normalised Citations"],
                              p["Year H"], p["Year M"],
                              p["Year Hnorm"], p["Year Mnorm"]))
            fp.write("\n")

        # Close
        fp.close()

# Compute collective statistics on each institution
keys = ["Total Citations", "Total Normalised Citations", "H", "M",
        "Hnorm", "Mnorm"]
shortkeys = ["cite", "ncite", "H", "M", "Hnorm", "Mnorm"]
keylabels = ["Citations", "Normalised Citations", r"$H$", r"$M$",
             r"$H_{\mathrm{norm}}$", r"$M_{\mathrm{norm}}$"]
logplot = [True, True, False, False, False, False]
ktype = ["d", "f", "d", "f", "d", "f"]
if args.years is not None:
    nk = len(keys)
    for i in range(nk):
        keys.append("Year "+keys[i])
        shortkeys.append("yr_"+shortkeys[i])
        ktype.append(ktype[i])
        keylabels.append(keylabels[i]+" ({:s} - {:s})".
                         format(args.years[0], args.years[1]))
        logplot.append(logplot[i])
for c in citedata:
    if args.Hmin > 0:
        Hlist = np.array([p["H"] for p in c["Personnel"]])
        idx = Hlist > args.Hmin
    else:
        idx = [True]*len(c["Personnel"])
    for k in keys:
        stat = np.array([p[k] for p in
                         np.array(c["Personnel"])[np.array(idx)]])
        stat.sort()
        statsort = stat[::-1]
        c[k] = statsort

# Write institutional comparison text file
for sk, k, kt in zip(shortkeys, keys, ktype):
    fp = open(args.outfile+"_"+sk+".txt", "w")
    nmax = np.amax([len(c["Personnel"]) for c in citedata])
    hdrstr = "Rank"
    for c in citedata:
        hdrstr += "   {:>10s}".format(c["Outfile"])
    fp.write(hdrstr+"\n")
    for i in range(nmax):
        outstr = "{:>4d}".format(i+1)
        for c in citedata:
            if kt == "d":
                if len(c[k]) > i:
                    outstr += "   {:10d}".format(int(c[k][i]))
                else:
                    outstr += "             "
            else:
                if len(c[k]) > i:
                    outstr += "   {:10.2f}".format(c[k][i])
                else:
                    outstr += "             "
        fp.write(outstr+"\n")
    fp.close()

# Make plots
colors = ['k', 'b', 'g', 'r', 'c', 'm']
#keys = ["Total Citations"]
if args.plot:
    for k, sk, kl, lp in zip(keys, shortkeys, keylabels, logplot):
        plt.figure(1, figsize=(7,4))
        plt.clf()
        lab = []
        p = []
        for j in range(len(citedata)):
            c = citedata[j]
            clr = colors[j]
            if j == 0 and args.plotemphasize:
                lw = 3
            else:
                lw = 1
            n = [1e-6]
            s = [c[k][0]]
            for i in range(len(c[k])-1):
                n.append(i)
                s.append(c[k][i])
                n.append(i)
                s.append(c[k][i+1])
            n.append(i+1)
            s.append(c[k][-1])
            plt.subplot(1,2,1)
            plt.plot(n, s, clr, lw=lw)
            plt.subplot(1,2,2)
            p1,=plt.plot(np.array(n, dtype=float)/len(c[k]), s, clr, lw=lw)
            p.append(p1)
            lab.append(c["Institution"])                
        plt.subplot(1,2,1)
        if lp:
            plt.yscale('log')
            yl = plt.gca().get_ylim()
            plt.ylim([max(1,yl[0]),yl[1]])
        plt.ylabel(kl)
        plt.xlabel('Staff')
        plt.legend(p, lab, loc='upper right', prop={'size' : 8})
        plt.subplot(1,2,2)
        if lp:
            plt.yscale('log')
            yl = plt.gca().get_ylim()
            plt.ylim([max(1,yl[0]),yl[1]])
        plt.gca().yaxis.set_ticklabels([])
        plt.xlabel('Staff fraction')
        plt.subplots_adjust(bottom=0.15, right=0.92, wspace=0.15)
        plt.savefig(args.outfile+"_"+sk+".pdf")
